<?php 
    include ('vendor/autoload.php');
    require_once ('connection.php');
    ob_start();
    session_start();
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login PDO</title>
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/assets/css/docs.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css">
</head>
<body>
    <?php 
        $check = false;
        $email = "";
        $password = "";

        // Kiểm tra định dạng email
        function validateEmail($email)
        {
            return (!preg_match("/^[a-zA-Z0-9]{6,225}\@[a-zA-Z0-9]{2,12}\.[a-zA-Z]{2,12}+$/", $email)) ? false : true;
        }

        // Kiểm tra định dạng password
        function validatePassword($password)
        {
             return (!preg_match("/^[A-Za-z0-9_\.]{6,50}+$/",$password)) ? false : true;
        }

        if( isset($_POST['Login'])) {
            $email = isset( $_POST['email']) ? $_POST['email'] : " ";
            $password = isset( $_POST['password']) ? $_POST['password'] : " " ;
            if ( empty($email)) {
                $errorEmail = "Bạn chưa nhập email";
            } else if ( !validateEmail($email)) {
                $errorEmail = "Email không đúng định dạng";
            }
            if ( empty($password)) {
                $errorPassword = "Bạn chưa nhập Password";
            } else if ( !validatePassword($password)) {
                $errorPassword = "Password phải có độ dài lớn hơn 6 ký tự và nhỏ hơn 50 ký tự";
            }
            $selectUser = $conn->prepare(" SELECT * FROM users");
            $selectUser->setFetchMode(PDO::FETCH_ASSOC);
            $selectUser->execute();
            while ( $results = $selectUser->fetch()) {
                if ( $results['mail_address'] == $_POST['email'] && $results['password'] == md5(trim($_POST['password'])) ) {
                    $_SESSION['email'] = $results['mail_address'];
                    header("location:LoginSuccessPdo.php");
                    break;
                }
            }
            echo "ĐĂNG NHẬP THẤT BẠI";
            if ( isset($_POST['remember'])) {
                setcookie('email', $email, time() + 3600);
                setcookie('password',$password, time() +3600);
            }
        }
        if ( isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
            $check = true ;
        }
     ?>
     <form action="LoginPdo.php" method="POST">
        <div class="container">
            <h1 style="text-align: center;">Login</h1>
            <div class="form-group">
                <label>Mail address</label>
                <input type="text" name="email" class="form-control" id="email" value="<?php if( isset( $_COOKIE['email'])) echo $_COOKIE['email'];  ?>">
                <?php echo isset($errorEmail) ? $errorEmail : "" ?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" id="password" value="<?php if( isset( $_COOKIE['password'])) echo $_COOKIE['password'];  ?>">
                <?php echo isset($errorPassword) ? $errorPassword : "" ?>   
            </div>
            <div class="form-group">
                 <input type="checkbox" name="remember"  id="remember" <?php echo ($check)?"checked":"" ?> > Remember me 
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="Login" value="Login">
            </div>
        </div>
    </form>
</body>
</html>