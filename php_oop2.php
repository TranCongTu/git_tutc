<?php 
    abstract class Supervisor
    {
        protected $slogan;
        abstract function saySloganOutLoud();
    }
    interface Boss
    {
        public function checkValidSlogan();
    }
    trait Active{
        public function defindYourSelf(){
            return get_class($this);
        }
    }
    class EasyBoss extends Supervisor implements Boss
    {   
        use Active;
        public function setSlogan($sloganContent)
        {
            $this->slogan = $sloganContent;
        }
        public function saySloganOutLoud(){
            echo $this->slogan;
        }
        public function checkValidSlogan()
        {
            return(strpos($this->slogan, 'after') || strpos($this->slogan, 'before'));
        }
    }
    class UglyBoss extends Supervisor implements Boss
    {   
        use Active;
        public function setSlogan($sloganContent)
        {
            $this->slogan = $sloganContent;
        }
        public function saySloganOutLoud(){
            echo $this->slogan;
        }
        public function checkValidSlogan()
        {
            return(strpos($this->slogan, 'after') && strpos($this->slogan, 'before'));
        }
    }
    $easyBoss = new EasyBoss();
    $uglyBoss = new UglyBoss();

    $easyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s)');

    $uglyBoss->setSlogan('Do NOT push anything to master branch before reviewed by supervisor(s). Only they can do it after check it all!');

    $easyBoss->saySloganOutLoud(); 
    echo "<br>";
    $uglyBoss->saySloganOutLoud(); 

    echo "<br>";

    var_dump($easyBoss->checkValidSlogan()); // true
    echo "<br>";
    var_dump($uglyBoss->checkValidSlogan()); // true

    echo "<br>";

    echo 'I am ' . $easyBoss->defindYourSelf(); 
    echo "<br>";
    echo 'I am ' . $uglyBoss->defindYourSelf(); 
 ?>