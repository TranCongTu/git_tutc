<?php 
    ob_start();
    session_start();
    $check = false;
    $email = "";
    $password = "";
    // Kiểm tra định dạng email
    function validateEmail($email)
    {
        return (!filter_var($email, FILTER_VALIDATE_EMAIL)) ? FALSE : TRUE;
    }

    // Kiểm tra định dạng password
    function validatePassword($password)
    {
       return (strlen($password) < 6 || strlen($password) > 50);
    }

    if( isset($_POST['Login'])) {
        $email = isset( $_POST['email']) ? $_POST['email'] : "";
        $password = isset( $_POST['password']) ? $_POST['password'] : "";
        if( empty($email)) {
            $errorEmail = "Bạn chưa nhập email";
        }else if( !validateEmail($email)) {
            $errorEmail = "Email không đúng định dạng";
        }
        if( empty($password)) {
            $errorPassword = "Bạn chưa nhập Password";
        }else if( validatePassword($password)){
            $errorPassword = "Password sai định dạng";
        }

        $conditionLogin = array(
            "email" => "tutc@gmail.com",
            "password" => "minhtu123456"
        );
        if( $_POST['email'] == $conditionLogin['email'] && $_POST['password'] == $conditionLogin['password']) {
            $_SESSION["userInfo"] = $conditionLogin;
            header("location:LoginSuccess.php");
        }else{
            echo "Đăng nhập thất bại";
        }
        if( isset($_POST['remember'])) {
            setcookie("email",$conditionLogin['email']);
            setcookie("password",$conditionLogin['password']);
        }
        if( isset($_COOKIE['email']) && isset($_COOKIE['password'])) {
            $email = $_COOKIE['email'];
            $password = $_COOKIE['password'];
            $check = true;
        }
    }
    if( isset($_POST['Huycookie']))
    {
        setcookie("email");
        setcookie("password");
    }
 ?>
<form method="POST" action="">
    <h1>Đăng Nhập</h1>
    <p>Email</p>
        <input type="text" name="email" id="email" value="<?php echo $email ?>">
        <?php echo isset($errorEmail) ? $errorEmail : "" ?>
        <br>
    <p>Password</p>
        <input type="password" name="password" id="password" value="<?php echo $password ?>">
        <?php echo isset($errorPassword) ? $errorPassword : "" ?>
        <br>
    <input type="checkbox" name="remember"  id="remember" <?php echo ($check) ? "checked" : "" ?> > Remember me <br><br>
    <input type="submit" name="Login" value="Login">
    <input type="submit" name="Huycookie" value="Huycookie">
</form>