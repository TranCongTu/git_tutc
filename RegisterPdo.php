<?php 
    include ('vendor/autoload.php');
    require_once ('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register page</title>
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/assets/css/docs.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-grid.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="vendor/twbs/bootstrap/dist/css/bootstrap-reboot.min.css">
</head>
<body>
    <?php 
        $error = array();
        // Kiểm tra định dạng email
        function validateEmail($email)
        {
            return (!preg_match("/^[a-zA-Z0-9]{6,230}\@[a-zA-Z0-9]{2,13}\.[a-zA-Z]{2,12}+$/", $email)) ? false : true;
        }

        // Kiểm tra định dạng password
        function validatePassword($password)
        {
            return (!preg_match("/^[A-Za-z0-9_\.]{6,50}+$/",$password)) ? false : true;
        }

        if ( isset($_POST['register'])) {
            $email = isset( $_POST['email']) ? $_POST['email'] : " ";
            $password = isset( $_POST['password']) ? $_POST['password'] : " ";
            $confirm_password = isset( $_POST['confirm_password']) ? $_POST['confirm_password'] : " ";
            if ( empty($email)) {
                $error['email'] = "Xin mời nhập Email";      
            } else if (!validateEmail($email)) {
                $error['email'] = "Email không đúng định dạng"; 
            } else if( strlen($email) > 255) {
                $error['email'] = "Email có độ dài không vượt quá 255 ký tự";
            }
            if ( empty($password)) {
                $error['password'] = "Xin mời nhập Password";
            } else if (!validatePassword($password)) {
                $error['password'] = "Password phải có độ dài lớn hơn 6 ký tự và nhỏ hơn 50 ký tự";
            }
            if ( empty($confirm_password)) {
                $error['confirm_password'] = " Xin mời bạn nhập lại Password ";
            } else if ($password != $confirm_password) {
                $error['confirm_password'] = " Password bạn vừa nhập không trùng khớp";
            }
            if (empty($error)) {
                $insertUser = $conn->prepare("INSERT INTO users (mail_address,password) VALUE (:email,:password)");
                $insertUser->bindParam(':email', $email);
                $insertUser->bindParam(':password', md5($password));
                $insertUser->execute();
                header("location:LoginPdo.php");
            }
        }
    ?>
    <form action="" method="POST">
        <div class="container">
            <h1 style="text-align: center;">Create account</h1>
            <div class="form-group">
                <label>Mail address</label>
                <input type="email" name="email" id="email" class="form-control" value="<?php echo isset($email)?$email:"" ?>" placeholder="Email">
                <?php echo isset($error['email']) ? $error['email'] : " "; ?>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" id="password" class="form-control" value="<?php echo isset($password)?$password:"" ?>" placeholder="Password">
                <?php echo isset( $error['password']) ? $error['password'] : " "; ?>
            </div>
            <div class="form-group">
                <label>Confirm password</label>
                <input type="password" name="confirm_password" id="confirm_password" class="form-control" value="<?php echo isset($confirm_password)?$confirm_password:"" ?>" placeholder="Password">
                <?php echo isset( $error['confirm_password']) ? $error['confirm_password'] : " "; ?>
            </div>
            <div class="form-group">
                <input type="submit" name="register" id="register" value="Register" class="btn btn-primary">
           </div>
        </div>    
    </form>
</body>
</html>