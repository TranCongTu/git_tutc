<?php 
    class HandleString 
    {
        public $check1 = true;
        public $check2 = true;

        public function readFile(string $file)
        {
            $stringInFile = "";
            $openFile = @fopen($file, "r");
            if (!$openFile) {
                echo 'Mở file không thành công';
            } else {
                while (!feof($openFile)) {
                    $stringInFile .= fgetc($openFile);
                }
            }
            return $stringInFile;
        }

        public function checkValidString(string $string) 
        {
            $charactersCount = strlen($string);
            if (empty($string)) {
                return true;
            }
            if ($charactersCount > 50 && strpos($string, 'after') === false ) {
                return true;
            }
            if(strpos($string, 'after') === false && strpos($string, 'before')) {
                return true;
            }
            return false;
        }
    }
    $object1 = new HandleString();
    $stringInFile1 = $object1 -> readFile('file1.txt');
    $checkString = $object1 -> checkValidString($stringInFile1);
    $check1 = $checkString;

    $stringInFile2 = $object1 -> readFile('file2.txt');
    $checkString = $object1 -> checkValidString($stringInFile2);
    $check2 = $checkString;
?>
