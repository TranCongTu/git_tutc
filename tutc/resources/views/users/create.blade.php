@extends('layouts.default')

@section('title', 'Thêm mới người dùng')

@section('content')
    <form action="{{ route('users.store') }}" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="container">
            <h1 style="text-align: center;">Create account</h1>
            <div class="form-group{{ $errors->has('mail_address') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label>Mail address</label>
                    <input type="email" name="mail_address" id="mail_address" class="form-control"  placeholder="Email">
                    @if ($errors->has('mail_address'))
                    <div class="alert alert-danger">
                        <span>{{ $errors->first('mail_address') }}</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">   
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="form-control"  placeholder="Password">
                    @if ($errors->has('password'))
                    <div class="alert alert-danger">
                        <span>{{ $errors->first('password') }}</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label>Confirm password</label>
                    <input type="password" name="confirm_password" id="confirm_password" class="form-control"  placeholder="Password">
                    @if ($errors->has('confirm_password'))
                    <div class="alert alert-danger">
                        <span>{{ $errors->first('confirm_password') }}</span>
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" id="name" class="form-control"  placeholder="Password">
                    @if ($errors->has('name'))
                        <div class="alert alert-danger">
                            <span>{{ $errors->first('name') }}</span>
                        </div>
                    @endif
                </div>              
            </div>
            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" name="address" id="address" class="form-control"  placeholder="Password">
                    @if ($errors->has('address'))
                        <div class="alert alert-danger">
                            <span>{{ $errors->first('address') }}</span>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                <div class="form-group">
                    <label>Phone</label>
                    <input type="text" name="phone" id="phone" class="form-control"  placeholder="Password">
                    @if ($errors->has('phone'))
                        <div class="alert alert-danger">
                            <span>{{ $errors->first('phone') }}</span>
                        </div>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label>Quyền truy cập</label>
                <select class="form-control" id="sel1" name="role">
                    <option value="1">Quản trị viên</option>
                    <option value="2">Nhân viên</option>
                </select>
            </div>
            <div class="form-group">
                <input type="submit" name="register" id="register" value="Register" class="btn btn-primary">
            </div>
        </div>    
    </form> 
@endsection
