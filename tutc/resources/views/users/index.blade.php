@extends('layouts.default')

@section('title', 'Danh sách người dùng')

@section('content')
    @include('flash::message');
    <form action="{{route('users.index')}}" method="GET">
        <div id="search_user">
            <div class="form-row">
                <div class="form-group col-xs-12 col-sm-4 col-md-6">
                    <input type="text" class="form-control" id="mail_address" placeholder="Email" name="mail_address" value="{{  old('mail_address') }}">
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-6">
                    <input type="text" class="form-control" id="name" placeholder="Tên" name="name" value="{{  old('name') }}">
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-6">
                    <input type="text" class="form-control" id="address" placeholder="Địa chỉ" name="address" value="{{  old('address') }}">
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-6">
                    <input type="text" class="form-control" id="phone" placeholder="Số điện thoại" name="phone" value="{{  old('phone') }}">
                </div>
                <div class="form-group col-xs-12 col-sm-4 col-md-1">
                    <input type="submit" name="search" class="btn btn-primary form-control" value="Tìm Kiếm">    
                </div>
            </div>   
        </div>
    </form>
    @can('admin')
        <a href="{{ route('users.create') }}" class="btn btn-primary" style="margin-bottom: 10px;"> {{ __('Thêm người dùng') }} </a>
    @endcan

    <table id="datatable" class="table table-striped table-bordered">
        <thead>
            <tr>
                <th>{{ __('STT') }}</th>
                <th>{{ __('Họ tên') }}</th>
                <th>{{ __('Email') }}</th>
                <th>{{ __('Điện thoại') }}</th>
                <th>{{ __('Địa chỉ') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $key => $user)
            <tr>
                @php $serial = $key + ($users->currentPage() - 1) * $users->perPage() + 1; @endphp
                <td>{{ number_format($serial) }}</td>
                <td>{{ Helper::toUpperCase($user->name) }}</td>
                <td>{{ $user->mail_address }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->address }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <div class="btn-group pull-right">
        {{ $users->render() }}
    </div>
@stop