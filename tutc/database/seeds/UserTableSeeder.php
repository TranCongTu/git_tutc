<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create('vi_VN');
        User::truncate();
        foreach (range(1,1) as $index) {
            User::create([
                'mail_address' => $faker->email,
                'name' => $faker->name,
                'password' => bcrypt('secret'),
                'address' => $faker->address,
                'phone' => $faker->phoneNumber,
                'role' => '2',
            ]);
        }
        User::create([
                'mail_address' =>'minhtu210894@gmail.com',
                'name' => 'Minh Tu',
                'password' => Hash::make('minhtu123456'),
                'address' => 'Thái Bình',
                'phone' => '01659313648',
                'role' => '1',
            ]);

    }
}
