<?php

namespace App\Helper;

class Helper 
{
    public function toUpperCase($strName)
    {
        return mb_strtoupper($strName);
    }
}