<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Pagination\LengthAwarePaginator;
use simplePaginate;
use Illuminate\Pagination\Paginator;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Jobs\SendRegisterEmail;
use Mail;


class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';

    protected $fillable = [
        'name','mail_address','password','address','phone', 'role'
    ];  

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     * 
     * @var array
     */
    protected $hidden = [
        'password','remember_token',
    ];

    protected $perPage = 20;

    public function getUser()
    {   
        $user = User::orderBy('mail_address','ASC')->paginate();
        return $user;
    }
    
    public function createUser(array $data)
    {
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        if (!empty($user)) {
            dispatch((new SendRegisterEmail($user)));
        }
        return $user;
    }

    public function searchUsers($request)
    {
        $email = $request->mail_address;
        $name = $request->name;
        $address = $request->address;
        $phone = $request->phone;
        if(empty($phone)) {
            $user = User::where([['mail_address','like',"%$email%"],['name','like',"%$name%"],['address','like',"%$address%"]])->orderBy('mail_address', 'ASC')->paginate();
            return $user;
        } else {
            $user = User::where([['mail_address','like',"%$email%"],['name','like',"%$name%"],['address','like',"%$address%"]])
            ->where('phone',"%$phone")->orderBy('mail_address', 'ASC')->paginate();
            return $user;
        }
    }
}
