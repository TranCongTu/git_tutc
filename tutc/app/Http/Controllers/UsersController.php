<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models as Database;
use App\Models\User;
use App\Http\Requests\CreateUserRequest;
use Helper;
use Laracasts\Flash\FlashServiceProvider;
// use App\Jobs\SendRegisterEmail;


class UsersController extends Controller
{   
    protected $modelUser;

    public function __construct(User $user) {
        $this->user = $user;
        $this->middleware('auth');
    } 
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        if(!empty($request->all())){
            $users = $this->user->searchUsers($request);
            return view('users.index',compact('users'));
        }else{
            $users = $this->user->getUser();
            return view('users.index',compact('users'));
        }
    }
    
     public function create()
    {
        $this->authorize('admin');
        return view('users.create');
    }

    public function store(CreateUserRequest $request, User $user)
    {
        $this->user->createUser($request->all());
        if (isset($this->user)) {
            flash('Thêm thành công')->success();
            return redirect()->route('users.index');
        } else {
            flash('Thêm thất bại')->error();
            return redirect()->back;
        }
    }
}
