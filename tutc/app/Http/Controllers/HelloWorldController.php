<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use app\Http\Controllers\Requests\UsersRequest;

class HelloWorldController extends Controller
{
    public function show()
    {
        return view('show');
    }
}
